import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  @Output() intervalFired = new EventEmitter<number>();
  interval: number;
  counter: number;
  constructor() {
    this.counter = 0;
   }

  ngOnInit(): void {
    this.interval = 0;
  }

  onStartPress(): void{
    this.interval = setInterval(() => {
      this.intervalFired.emit(this.counter + 1);
      this.counter += 1;
    }, 1000);
  }

  onStopPress(): void{
    clearInterval(this.interval);
  }

}
