import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-data-binding-trainning';
  oddNumber: number[] = [];
  evenNumber: number[] = [];

  onIntervalFired(event: number): void{
    if (event % 2 === 0){
      this.evenNumber.push(event);
    } else {
      this.oddNumber.push(event);
    }
  }
}
