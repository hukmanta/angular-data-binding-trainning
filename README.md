# angular data binding trainning

Bind It!
1. create three new components: GameControl, Odd, and Even
2. The GameControl Component should have buttons to start and stop the game
3. When staring the game, an event(holding a incrementing number) should get emmited each second (ref=setInterval())
4. The Event Should be listenable from outside the component
5. When Stopping the game, no more event should get emitted (clearInterval(ref))
6. A new Odd component should get created for every odd number emitted, the same should happend for the even component(on even numbers)
7. Simply output Odd -  NUMBER or Even -Number in the two components
8. Style the element (e.g. paragraph) holding your output text differently in both components
